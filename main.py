""" Simple Network Inferface Monitoring application """
import os
import flask
from flask import jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

data = {}

def get_network_data( type_, info_, interface_='eth0' ):
    """ Function reading stats from linux machine """
    # METRICS_PATH is needed for docker to get host metrics:
    metrics_path = os.getenv('METRICS_PATH', '/sys/class/net')
    network_file = open("{}/{}/statistics/{}_{}".format(metrics_path, interface_, type_, info_))
    metric = network_file.read().split('\n')[0]
    network_file.close()
    return metric

def build_metrics():
    """ Function preparing json with metrics data """
    data['RX_packets'] = get_network_data('rx','packets')
    data['RX_bytes'] = get_network_data('rx','bytes')
    data['RX_errors'] = get_network_data('rx','errors')

    data['TX_packets'] = get_network_data('tx','packets')
    data['TX_bytes'] = get_network_data('tx','bytes')
    data['TX_errors'] = get_network_data('tx','errors')
    return data

@app.route('/api', methods=['GET'])
def home():
    """ API General Page """
    return "<h1>API</h1><p>This site is a prototype API for getting network interface metrics.</p>"

@app.route('/api/interface', methods=['GET'])
def api():
    """ Calling this endpoint, we get the required data for the task """
    return jsonify(data)

@app.route('/api/interface/reload', methods=['GET'])
def reload():
    """ API for manual data reload and redirect to main page with refreshed statistics """
    build_metrics()
    return flask.redirect("/api/interface")

@app.route('/api/interface/autoreload', methods=['GET'])
def auto_reload():
    """ Endpoint used for automatic page refresh, can be used in k8s healtcheck as well. """
    build_metrics()
    return ('', 204)

build_metrics()
app.run(host = '0.0.0.0', port=80)
