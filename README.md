# Network Monitor
A simple python application watching RX/TX packets, bytes and errors of the default network interface with 30s refresh date.
Metrics are collected from linux macine statistics - "/sys/class/net/eth0/statistics/{RX/TX}_{packets/bytes/errors}"

## How to start the application
The application can run inside docker container or locally.

### Local Run
```
pip3 -r requirements
python3 main.py
```

### Docker Run
When running inside of docker container its important to mount hosts networking stats
```
docker build . -t network-monitor:latest
```
collect docker metrics: 
```
docker run -p 80:80 network-monitor:latest
```
collect AWS EC2 metrics (Amazon Linux 2):
```
docker run -p 80:80 -e METRICS_PATH=/metrics -v /sys/devices/vif-0/net:/metrics network-monitor:latest
```
Local Metrics - check your PC symlink of "/sys/class/net" and atach it as volume to docker container.

### Environment Variables
```
DISABLE_METRICS_RELOAD=true - stops automatic refresh ot metrics
REFRESH_SECOND=30 - setup metrics refresh rate (default 30)
METRICS_PATH=/metrics - redirecting container to watch for host metrics, should add them as volume
```

### API
General API information
```
 GET /api
 ```

Get default network interface metrics
```
 GET /api/interface
```

Manual reload of metrics with redirect to default interface page
```
 GET /api/interface/reload
```

Automatic reload of metrics
```
 GET /api/interface/autoreload
```

## CI/CD
The pipeline uses the following variables:
* IMAGE - docker image name, can be set in gitlab-ci file
* REPOSITORY_USERNAME - user name for docker repository, set as CI environment variable
* REPOSITORY_PASSWORD - password for docker repository, set as masked CI environment variable
* REPOSITORY_URL - repository url, set as CI environment variable
* REPOSITORY_URL_TAG - might be for additional tagging, for example namespace in repository. ${REPOSITORY_URL}/<namespace>
* K8S_NAMESPACE - kubernetes namespace, where to deploy
* K8S_API_SERVER - kubernetes api server for deploy, set as CI environment variable
* K8S_TOKEN - kubernetes bearer token for deployment, set as masked CI environment variable
