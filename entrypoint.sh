#/bin/sh

# Add metrics reload cronjob, option for disable.
# Kubernetes Liveness check can take the role for the autorefresh
if [ -z $DISABLE_METRICS_RELOAD ]; then
    while true; sleep ${REFRESH_SECOND}s; do curl -s http://localhost/api/interface/autoreload > /dev/nul; done & # hitting reload EP in infinite loop
fi

# Start python application
python3 main.py