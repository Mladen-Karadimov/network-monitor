FROM python:3.9-alpine

WORKDIR /app
RUN apk add --no-cache curl
COPY requirements.txt .
RUN pip3 install -r requirements.txt

ENV REFRESH_SECOND 30

COPY . .

CMD ["/bin/sh","entrypoint.sh"]